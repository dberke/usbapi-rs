# 0.3.0

 - Change ControlTransfer API and setup an mmap fort control transfers.
 - get_descriptor_string variants now return error on fails.

# 0.2.4

 - Fix some of the unsafe memory leaks
 - Use mmap for control channel.
 - Remove some code better panic that seg fault. See readme for details what need fixes.

